output:
	go build
	./www-archiver&
	x-www-browser localhost:9001

docker:
	docker build . -t www-archiver
	docker run -d --name www-archiver-docker -p 9001:9001 www-archiver

clean-docker:
	docker stop www-archiver-docker
	docker rm www-archiver-docker

clean:
	killall www-archiver
	rm -rf www-archiver
