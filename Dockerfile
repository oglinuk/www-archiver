FROM golang:1.15.0
ADD . /go/src/www-archiver
WORKDIR /go/src/www-archiver
RUN go build
EXPOSE 9001
CMD ["./www-archiver"]
