package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	goccer "github.com/OGLinuk/goccer/include"
	"github.com/gin-gonic/contrib/cors"
	"github.com/gin-gonic/gin"
)

var (
	tpl            *template.Template
	timeComplexity time.Time
	wp             *goccer.WorkerPool
	// TODO: Set dynamically
	filters = map[string]struct{}{
		"facebook":  {},
		"instagram": {},
		"google":    {},
		"youtube":   {},
		"amazon":    {},
		"microsoft": {},
		"apple":     {},
	}
)

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))

	logFile, err := os.OpenFile("logs.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("main.go::init::os.OpenFile::ERROR: %s", err.Error())
	}

	mw := io.MultiWriter(os.Stdout, logFile)

	log.SetOutput(mw)

	wp = goccer.NewWorkerPool(filters)
	wp.InitProducer()
}

func main() {
	HOST := os.Getenv("UI_HOST")
	PORT, err := strconv.ParseInt(os.Getenv("UI_PORT"), 10, 64)
	if err != nil {
		PORT = 9001
	}

	if HOST == "" {
		HOST = "0.0.0.0"
	}

	g := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	g.Use(cors.New(corsConfig))

	g.GET("/", indexHandler)
	g.POST("/", indexHandler)
	g.GET("/crawl", indexHandler)
	g.GET("/download", downloadHandler)
	g.StaticFS("/view", http.Dir("./static/resources"))
	g.StaticFS("/static", http.Dir("./static"))

	if err := g.Run(fmt.Sprintf("%s:%d", HOST, PORT)); err != nil {
		log.Fatalf("Failed to run gin server: %v", err)
	}
}
