module gitlab.com/OGLinuk/www-archiver

go 1.13

require (
	github.com/OGLinuk/goccer v0.0.0-20201213040109-0485a08033f6
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.6.3
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
)
