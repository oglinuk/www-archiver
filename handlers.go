package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type Payload struct {
	Seed      string
	URLs      []string
	Collected int
	Time      string
}

// TODO: Needs refactoring
func indexHandler(ctx *gin.Context) {
	var collected []string

	if ctx.Request.Method == "POST" {
		var payload *Payload

		/* Download the requested url */
		// https://en.wikipedia.org/Chaos_Theory
		// en.wikipedia.org+wiki+Chaos_Theory
		//
		rawURL := ctx.Request.FormValue("url")
		log.Printf("[handlers.go]::rawURL: %s", rawURL)

		URL, err := url.Parse(rawURL)
		if err != nil {
			log.Printf("url.Parse::ERROR: %s\n", err.Error())
			http.Error(ctx.Writer, fmt.Sprintf("url.Parse::ERROR: %s", err.Error()), 500)
		}

		parsed := strings.ReplaceAll(fmt.Sprintf("%s%s", URL.Hostname(), URL.Path), "\"", "")

		root := strings.ReplaceAll(parsed, "/", "+")

		// TODO: Replace with downloadHandler functionality
		r, err := http.Get(fmt.Sprintf("http://0.0.0.0:9001/download?url=\"%s\"", rawURL))
		if err != nil {
			log.Printf("http.Get::ERROR: %s\n", err.Error())
			http.Error(ctx.Writer, fmt.Sprintf("http.Get::ERROR: %s", err.Error()), 500)
		}
		defer r.Body.Close()

		if !strings.HasPrefix(parsed, "http") {
			parsed = fmt.Sprintf("https://%s", parsed)
		}
		if strings.Contains(parsed, "+") {
			parsed = strings.ReplaceAll(parsed, "+", "/")
		}

		timeComplexity = time.Now()

		collected = wp.Queue([]string{parsed})
		log.Printf("[handlers.go]::wp.Queue::size: %d", len(collected))

		payload = &Payload{
			Seed:      root,
			URLs:      collected,
			Collected: len(collected),
			Time:      fmt.Sprintf("%s", time.Since(timeComplexity)),
		}
		sort.Strings(payload.URLs)
		log.Printf("Payload.URLs: %d\n", len(payload.URLs))

		err = tpl.ExecuteTemplate(ctx.Writer, "index.html", payload)
		if err != nil {
			log.Printf("tpl.ExecuteTemplate::ERROR: %s\n", err.Error())
			http.Error(ctx.Writer, fmt.Sprintf("tpl.ExecuteTemplate::ERROR: %s", err.Error()), 500)
		}
	} else {
		// When /crawl/:root is given
		root := ctx.Query("root")
		if root != "" {
			if !strings.Contains(root, "http") {
				root = fmt.Sprintf("https://%s", root)
			}
			log.Printf("handlers.go::indexHandler::ctx.Query(root):%s", root)

			// TODO: Replace with downloadHandler functionality
			r, err := http.Get(fmt.Sprintf("http://0.0.0.0:9001/download?url=\"%s\"", root))
			if err != nil {
				log.Printf("http.Get::ERROR: %s\n", err.Error())
				http.Error(ctx.Writer, fmt.Sprintf("http.Get::ERROR: %s", err.Error()), 500)
			}
			defer r.Body.Close()

			timeComplexity = time.Now()
			collected = wp.Queue([]string{root})
			log.Printf("[handlers.go]::wp.Queue::size: %d", len(collected))
		}

		payload := &Payload{
			Seed:      root,
			URLs:      collected,
			Collected: len(collected),
			Time:      fmt.Sprintf("%s", time.Since(timeComplexity)),
		}

		err := tpl.ExecuteTemplate(ctx.Writer, "index.html", payload)
		if err != nil {
			http.Error(ctx.Writer, fmt.Sprintf("tpl.ExecuteTemplate::ERROR: %s", err.Error()), 500)
		}
	}
}

// TODO: Needs refactoring
func downloadHandler(ctx *gin.Context) {
	URL := ctx.Query("url")
	URL = strings.ReplaceAll(URL, "\"", "")

	/* Open a local file (<name>.html) and write the contents of the http.Get resp to it */

	// Parse the filtered URL
	parsed, err := url.Parse(URL)
	if err != nil {
		http.Error(ctx.Writer, fmt.Sprintf("Failed to url.Parse: %s", err.Error()), 500)
	}

	// Preserve the original source link within the filename along with checksum
	// ('.' -> '_') ('/' -> '-')
	var name string

	name = fmt.Sprintf("%s%s", parsed.Hostname(), parsed.Path)

	if strings.HasSuffix(name, "/") {
		name = strings.TrimSuffix(name, "/")
	}

	name = strings.ReplaceAll(name, "/", "-")
	name = strings.ReplaceAll(name, ".", "_")

	// Ensure the file extension is .html
	// TODO: Add check to ensure last char is not a symbol ('.', ',', '/', ...)
	// TODO: How to deal with edgecase like /view/S.M.A.R.T..html
	if !strings.HasSuffix(name, ".html") {
		name = fmt.Sprintf("%s.html", name)
	}

	f, err := os.Create(fmt.Sprintf("./static/resources/%s", name))
	if err != nil {
		http.Error(ctx.Writer, fmt.Sprintf("os.OpenFile::ERROR: %s", err.Error()), 500)
	}
	defer f.Close()

	// Ensure URL has http at this point
	if !strings.HasPrefix(URL, "http") {
		URL = fmt.Sprintf("https://%s", URL)
	}

	resp, err := http.Get(URL)
	if err != nil {
		http.Error(ctx.Writer, fmt.Sprintf("http.Get::ERROR: %s", err.Error()), 500)
	}
	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(ctx.Writer, fmt.Sprintf("ioutil.ReadAll::ERROR: %s", err.Error()), 500)
	}

	f.Write(bytes)

	log.Printf("Successfully saved %s ...\n", name)

	// TODO: Refactor to use env
	ctx.Redirect(http.StatusMovedPermanently, fmt.Sprintf("http://localhost:9001/static/resources/%s", name))
}
